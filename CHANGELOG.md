<a name="unreleased"></a>
## [Unreleased]


<a name="0.1.6"></a>
## [0.1.6] - 2019-12-09
### Update
- update git url


<a name="0.1.5"></a>
## [0.1.5] - 2019-11-15
### Change
- change default right on config file


<a name="0.1.4"></a>
## [0.1.4] - 2019-10-24
### Add
- add a .gitignore

### Remove
- remove version for molecule test
- remove version from meta


<a name="0.1.3"></a>
## [0.1.3] - 2019-10-14
### Add
- add explicit install of python3 and python3 devel

### Release
- Release 0.1.3

### Use
- use python3 as default


<a name="0.1.2"></a>
## [0.1.2] - 2019-10-11
### Add
- add setuptools-scm as pip dep

### Allow
- allow extra args for num install command

### Disable
- Disable Fedora test as it s...

### Fix
- fix config list of partition

### Release
- Release 0.1.2


<a name="0.1.1"></a>
## [0.1.1] - 2019-10-04
### Add
- add a step to create config directory
- add config template

### Fix
- fix only pip version, as it look like other have indepotent issue
- fix config variable name
- fix pip and setuptools version

### Release
- Release 0.1.1


<a name="0.1.0"></a>
## [0.1.0] - 2019-10-04
### Add
- Add License

### Release
- Release 0.1.0


<a name="0.0.3"></a>
## [0.0.3] - 2019-10-04
### Add
- add num pip install + split deps other yml
- add setup dist generation

### Fix
- fix syntax

### Get
- get num current version


<a name="0.0.2"></a>
## [0.0.2] - 2019-10-04
### Add
- Add basic molecule test

### Fix
- fix meta syntax


<a name="0.0.1"></a>
## 0.0.1 - 2019-10-04
### Add
- add epel for CentOS
- add some meta about package
- add python tools installation

### Download
- download source from git + use pip to install tools

### First
- First Commit

### Update
- Update Readme


[Unreleased]: https://gitlab.com/ifb-elixirfr/ansible-roles/ansible-num/compare/0.1.6...HEAD
[0.1.6]: https://gitlab.com/ifb-elixirfr/ansible-roles/ansible-num/compare/0.1.5...0.1.6
[0.1.5]: https://gitlab.com/ifb-elixirfr/ansible-roles/ansible-num/compare/0.1.4...0.1.5
[0.1.4]: https://gitlab.com/ifb-elixirfr/ansible-roles/ansible-num/compare/0.1.3...0.1.4
[0.1.3]: https://gitlab.com/ifb-elixirfr/ansible-roles/ansible-num/compare/0.1.2...0.1.3
[0.1.2]: https://gitlab.com/ifb-elixirfr/ansible-roles/ansible-num/compare/0.1.1...0.1.2
[0.1.1]: https://gitlab.com/ifb-elixirfr/ansible-roles/ansible-num/compare/0.1.0...0.1.1
[0.1.0]: https://gitlab.com/ifb-elixirfr/ansible-roles/ansible-num/compare/0.0.3...0.1.0
[0.0.3]: https://gitlab.com/ifb-elixirfr/ansible-roles/ansible-num/compare/0.0.2...0.0.3
[0.0.2]: https://gitlab.com/ifb-elixirfr/ansible-roles/ansible-num/compare/0.0.1...0.0.2
