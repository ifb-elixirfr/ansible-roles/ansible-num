Num
===

A role to install Num from https://gitlab.com/ifb-elixirfr/cluster/num

Dependencies
------------

[Requirements](molecule/default/requirements.yml)


Variables
---------

[Role Variables](defaults/main.yml)

[Dependencie Variables](vars/main.yml)


Example Playbook
----------------

[Test Playbook](molecule/default/playbook.yml)


License
-------

[License](LICENSE)
